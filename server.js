const express = require("express");
const todoRoutes = require("./src/todo/routes");
const app = express();
const handleServerError = (err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send('Server Error');
}
app.use(express.json());
app.get("/", (req, res) => {
  res.send("Home page")
});
app.use("/todos", todoRoutes);
app.use(handleServerError);
app.listen(process.env.PORT, () => {
  console.log(`Server started on port ${process.env.PORT}`);
});
