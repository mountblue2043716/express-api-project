const yup = require("yup");
const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const todoSchema = yup.object().shape({
  text: yup.string().required(),
  isCompleted: yup.boolean().default(false),
});
const paramsSchema = yup.object().shape({
  id: yup.number().required(),
});

// 1. Get Todo List API
const getTodos = async (req, res) => {
  try {
    const data = await prisma.todos.findMany();
    res.status(200).json(data);
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

// 2.Get Todo Detail API
const getUniqueTodo = async (req, res) => {
  try {
    await paramsSchema.validate(req.params);
  } catch (error) {
    return res.status(400).json({ message: error.message });
  }
  try {
    const data = await prisma.todos.findUnique({
      where: {
        id: parseInt(req.params.id),
      },
    });
    if (data === null) {
      return res.status(404).json({ message: "Todo not found" });
    }
    res.status(200).json(data);
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

// 3. Create Todo API:
const createTodo = async (req, res) => {
  try {
    await todoSchema.validate(req.body);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
  try {
    const data = await prisma.todos.create({
      data: {
        text: req.body.text,
        isCompleted: req.body.isCompleted,
      },
    });
    res.status(201).json(data);
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

// 4. Update Todo API:
const updateTodo = async (req, res) => {
  try {
    await todoSchema.validate(req.body);
    await paramsSchema.validate(req.params);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
  try {
    
    const todo = await prisma.todos.findUnique({
      where: {
        id: parseInt(req.params.id),
      },
    });
    if (todo === null) {
      return res.status(404).json({ message: "Todo not found" });
    }
    const data = await prisma.todos.update({
      where: {
        id: parseInt(req.params.id),
      },
      data: {
        text: req.body.text,
        isCompleted: req.body.isCompleted,
      },
    });
    res.status(200).json(data);
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

// 5. Delete Todo
const deleteTodo = async (req, res) => {
  try {
    await paramsSchema.validate(req.params);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
  try {
    const deleteTodo = await prisma.todos.delete({
      where: {
        id: parseInt(req.params.id),
      },
    });
    res.status(200).json({ message: "Todo deleted successfully" });
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

module.exports = {
  getTodos,
  getUniqueTodo,
  createTodo,
  updateTodo,
  deleteTodo,
};
