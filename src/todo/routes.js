const { Router} = require("express");
const controller = require("./controller");
const router = Router();

router.get("/", controller.getTodos);
router.get("/:id", controller.getUniqueTodo);
router.post("/", controller.createTodo);
router.put("/:id", controller.updateTodo);
router.delete("/:id", controller.deleteTodo);

module.exports = router;