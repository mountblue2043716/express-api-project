# Express JS CRUD API Project

Five API's have been created in this project which performs CRUD operations. 


## Prerequisites

- Node.js
- npm
- PostgreSQL


## Clone from GitLab

Run the following command:

```bash
git clone git@gitlab.com:mountblue2043716/express-api-project.git
```


## Setup procedure

- Navigate to project directory

- Install dependencies by running following command:

```bash
npm install
```

- Postgres database setup

After loging in to Postgres server using your user name and password,run the command:
```sql
CREATE DATABASE <dbname>;
```

- Configure Environment variables:
Update .env file similar to example.env


- Run the following command to add table to your database:

```bash
npx prisma migrate dev
```

- Run the server :

```bash
node server.js
```








